package mm.appstud.com.appstud_molette_mathieu.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import mm.appstud.com.appstud_molette_mathieu.AppStud;
import mm.appstud.com.appstud_molette_mathieu.R;
import mm.appstud.com.appstud_molette_mathieu.helpers.VolleyHelper;
import mm.appstud.com.appstud_molette_mathieu.interfaces.IRockstarListener;
import mm.appstud.com.appstud_molette_mathieu.managers.RockstarsManager;
import mm.appstud.com.appstud_molette_mathieu.models.Rockstar;

public class RockstarsRecyclerViewAdapter extends RecyclerView.Adapter<RockstarsRecyclerViewAdapter.ViewHolder> implements View.OnClickListener, Filterable {

    private static final int TYPE_ROCKSTAR = 1;
    private static final int TYPE_FAVORITE = 2;

    IRockstarListener mListener;

    private List<Rockstar> mValues;
    ImageLoader mImageLoader;
    private final static String basePath = AppStud.getContext().getString(R.string.basePath);
    private String imageUrl;
    private Boolean isFavoriteFrag = false;

    public RockstarsRecyclerViewAdapter(List<Rockstar> items, Boolean isFavoriteFrag, IRockstarListener listener) {
        mValues = items;
        this.isFavoriteFrag = isFavoriteFrag;
        mListener = listener;
    }

    @Override
    public int getItemViewType(int position) {
        int type = TYPE_ROCKSTAR;
        if(isFavoriteFrag){
            type = TYPE_FAVORITE;
        }

        return type;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        int layout = (viewType == TYPE_FAVORITE)? R.layout.fragment_rockstar_favorite : R.layout.fragment_rockstar;
        View view = LayoutInflater.from(parent.getContext())
                .inflate(layout, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.mItem = mValues.get(position);

        // Get the ImageLoader
        mImageLoader = VolleyHelper.getInstance(AppStud.getContext()).getImageLoader();
        imageUrl = basePath + holder.mItem.getPicture();
        holder.mPictureView.setImageUrl(imageUrl, mImageLoader);

        holder.mLastnameView.setText(holder.mItem.getLastname());
        holder.mFirstnameView.setText(holder.mItem.getFirstname());
        holder.mStatusView.setText(holder.mItem.getStatus());

        holder.mActionButton.setTag(holder.mItem);

        if(RockstarsManager.getInstance().isFavorite(holder.mItem)){
            int actionDrawable = R.drawable.ic_star_black_24dp;
            if(holder.getItemViewType() == TYPE_FAVORITE){
                actionDrawable = R.drawable.ic_delete_forever_black_24dp;
            }
            holder.mActionButton.setImageDrawable(AppStud.getContext().getResources().getDrawable(actionDrawable));
        }else{
            holder.mActionButton.setImageDrawable(AppStud.getContext().getResources().getDrawable(R.drawable.ic_star_border_black_24dp));
        }
        holder.mActionButton.setOnClickListener(this);
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){

            case R.id.actionButton:

                Rockstar rockstar = (Rockstar) v.getTag();
                if(!isFavoriteFrag){
                    if(RockstarsManager.getInstance().addFavorite(rockstar)){
                        ((ImageButton) v).setImageDrawable(AppStud.getContext().getResources().getDrawable(R.drawable.ic_star_black_24dp));
                        mListener.onRockstarBookmarked(rockstar);
                    }else{
                        if(RockstarsManager.getInstance().isFavorite(rockstar)){
                            Toast.makeText(AppStud.getContext(), AppStud.getContext().getString(R.string.already_fav), Toast.LENGTH_SHORT).show();
                        }else{
                            Toast.makeText(AppStud.getContext(), AppStud.getContext().getString(R.string.error_add_fav), Toast.LENGTH_SHORT).show();
                        }
                    }

                }else{
                    if(RockstarsManager.getInstance().removeFavorite(rockstar)){
                        this.mValues.remove(rockstar);
                        this.notifyDataSetChanged();
                        mListener.onRockstarBookmarked(rockstar);
                    }else{
                        Toast.makeText(AppStud.getContext(), AppStud.getContext().getString(R.string.error_delete_fav), Toast.LENGTH_SHORT).show();
                    }
                }

                break;
        }
    }

    @Override
    public Filter getFilter() {
        return new RockstarFilter(this, RockstarsManager.getInstance().getRockstarsList());
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final NetworkImageView mPictureView;
        public final TextView mLastnameView;
        public final TextView mFirstnameView;
        public final TextView mStatusView;
        public final ImageButton mActionButton;
        public Rockstar mItem;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            mPictureView = (NetworkImageView) view.findViewById(R.id.rockstarPicture);
            mLastnameView = (TextView) view.findViewById(R.id.lastnameView);
            mFirstnameView = (TextView) view.findViewById(R.id.firstnameView);
            mStatusView = (TextView) view.findViewById(R.id.statusView);
            mActionButton = (ImageButton) view.findViewById(R.id.actionButton);
        }

        @Override
        public String toString() {

            return super.toString() + " '" + mLastnameView.getText() + "'";
        }
    }

    // Clean all elements of the recycler
    public void clear() {
        mValues.clear();
        notifyDataSetChanged();
    }

    // Add a list of items
    public void addAll(List<Rockstar> list) {
        mValues.addAll(list);
        notifyDataSetChanged();
    }

    private static class RockstarFilter extends Filter {

        private final RockstarsRecyclerViewAdapter adapter;

        private final List<Rockstar> originalList;

        private final List<Rockstar> filteredList;

        private RockstarFilter(RockstarsRecyclerViewAdapter adapter, List<Rockstar> originalList) {
            super();
            this.adapter = adapter;
            this.originalList = new LinkedList<>(originalList);
            this.filteredList = new ArrayList<>();
        }

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            filteredList.clear();
            final FilterResults results = new FilterResults();

            if (constraint.length() == 0) {
                RockstarsManager.getInstance().resetFilteredList();
            } else {
                final String filterPattern = constraint.toString().toLowerCase().trim();
                // Filter on lastname & firstname
                for (final Rockstar rockstar : originalList) {
                    if (rockstar.getLastname().toLowerCase().contains(filterPattern) || rockstar.getFirstname().toLowerCase().contains(filterPattern)) {
                        filteredList.add(rockstar);
                    }
                }
            }
            results.values = filteredList;
            results.count = filteredList.size();
            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            // Refresh list after filter
            adapter.clear();
            adapter.addAll((ArrayList<Rockstar>) results.values);
        }
    }
}
