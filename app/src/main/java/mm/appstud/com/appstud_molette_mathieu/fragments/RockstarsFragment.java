package mm.appstud.com.appstud_molette_mathieu.fragments;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.android.volley.Response;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import mm.appstud.com.appstud_molette_mathieu.AppStud;
import mm.appstud.com.appstud_molette_mathieu.R;
import mm.appstud.com.appstud_molette_mathieu.activities.MainActivity;
import mm.appstud.com.appstud_molette_mathieu.adapters.RockstarsRecyclerViewAdapter;
import mm.appstud.com.appstud_molette_mathieu.dao.RockstarDao;
import mm.appstud.com.appstud_molette_mathieu.helpers.DatabaseHelper;
import mm.appstud.com.appstud_molette_mathieu.interfaces.IRockstarListener;
import mm.appstud.com.appstud_molette_mathieu.managers.RockstarsManager;
import mm.appstud.com.appstud_molette_mathieu.models.Rockstar;
import mm.appstud.com.appstud_molette_mathieu.services.RockstarServices;
import mm.appstud.com.appstud_molette_mathieu.tasks.RefreshRockstarsAsyncTask;

public class RockstarsFragment extends Fragment {

    IRockstarListener mListener;

    public static final String BUNDLE_FAV = "favorite";
    private RockstarsRecyclerViewAdapter recyclerViewAdapter;
    private SwipeRefreshLayout swipeContainer;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public RockstarsFragment() {}

    public static RockstarsFragment newInstance() {
        RockstarsFragment fragment = new RockstarsFragment();

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        recyclerViewAdapter = new RockstarsRecyclerViewAdapter(RockstarsManager.getInstance().getFilteredList(), false, mListener);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_rockstar_list, container, false);
        RecyclerView recyclerView = (RecyclerView) view.findViewById(R.id.rockstarRecycler);

        // Set the adapter
        Context context = recyclerView.getContext();
        recyclerView.setLayoutManager(new LinearLayoutManager(context));
        recyclerView.setAdapter(recyclerViewAdapter);


        // Swipe refresh
        swipeContainer = (SwipeRefreshLayout) view;

        // Setup refresh listener which triggers new data loading
        swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {

            @Override
            public void onRefresh() {
                Response.Listener<JSONObject> resListener = new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d("onResponse", response.toString());

                        RefreshRockstarsAsyncTask refreshRockstarsAsyncTask = new RefreshRockstarsAsyncTask(response, recyclerViewAdapter, swipeContainer);
                        refreshRockstarsAsyncTask.execute();

                    }
                };

                // Launch request
                RockstarsManager.getInstance().loadRockstars(resListener);
            }
        });


        return view;
    }

    public RockstarsRecyclerViewAdapter getRecyclerViewAdapter(){
        return recyclerViewAdapter;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            mListener = (IRockstarListener) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString()
                    + " must implement IRockstarListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

}
