package mm.appstud.com.appstud_molette_mathieu.interfaces;

import java.util.List;

import mm.appstud.com.appstud_molette_mathieu.models.Rockstar;

/**
 * Created by Mathieu on 02/04/2016.
 */
public interface IRockstarDao {
    public Rockstar fetchRockstarById(int userId);
    public List<Rockstar> fetchAllRockstars();
    public boolean addRockstar(Rockstar rockstar);
    public boolean deleteRockstar(Rockstar rockstar);
}
