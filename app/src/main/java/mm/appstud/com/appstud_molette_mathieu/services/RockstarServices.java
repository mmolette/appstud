package mm.appstud.com.appstud_molette_mathieu.services;

import android.util.Log;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import mm.appstud.com.appstud_molette_mathieu.AppStud;
import mm.appstud.com.appstud_molette_mathieu.R;
import mm.appstud.com.appstud_molette_mathieu.activities.SplashscreenActivity;
import mm.appstud.com.appstud_molette_mathieu.managers.RockstarsManager;
import mm.appstud.com.appstud_molette_mathieu.models.Rockstar;

/**
 * Created by Mathieu on 31/03/2016.
 */
public class RockstarServices {

    private static final String basePath = AppStud.getContext().getString(R.string.basePath);
    private static final String rockstarsUrl = basePath + AppStud.getContext().getString(R.string.contactsService);

    public static JsonObjectRequest getRockstarsRequest(Response.Listener<JSONObject> listener){

        JsonObjectRequest rockstarsRequest = new JsonObjectRequest(Request.Method.GET, rockstarsUrl, null, listener, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("RockstarsManager onErrorResponse", "Error: " + error.getMessage());
                Toast.makeText(AppStud.getContext(), "Error while loading rockstars", Toast.LENGTH_LONG).show();

            }
        });

        return rockstarsRequest;
    }
}
