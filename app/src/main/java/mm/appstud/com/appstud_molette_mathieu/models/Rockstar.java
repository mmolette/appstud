package mm.appstud.com.appstud_molette_mathieu.models;

import android.graphics.Bitmap;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Mathieu on 24/03/2016.
 */
public class Rockstar {

    private final String FIRSTNAME_PROP = "firstname";
    private final String LASTNAME_PROP = "lastname";
    private final String STATUS_PROP = "status";
    private final String PICTURE_PROP = "hisface";

    private Long id;
    private String picture;
    private String lastname;
    private String firstname;
    private String status;

    public Rockstar() {
    }

    public Rockstar(String picture, String lastname, String firstname, String status) {
        this.picture = picture;
        this.lastname = lastname;
        this.firstname = firstname;
        this.status = status;
    }

    public Rockstar(JSONObject object) throws JSONException {
        if (object.has(FIRSTNAME_PROP)) {
            this.firstname = object.getString(FIRSTNAME_PROP);
        }
        if (object.has(LASTNAME_PROP)) {
            this.lastname = object.getString(LASTNAME_PROP);
        }
        if (object.has(STATUS_PROP)) {
            this.status = object.getString(STATUS_PROP);
        }
        if (object.has(PICTURE_PROP)) {
            this.picture = object.getString(PICTURE_PROP);
        }
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
