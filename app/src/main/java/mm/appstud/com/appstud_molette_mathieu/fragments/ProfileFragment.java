package mm.appstud.com.appstud_molette_mathieu.fragments;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import java.io.BufferedInputStream;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;

import mm.appstud.com.appstud_molette_mathieu.R;

public class ProfileFragment extends Fragment implements View.OnClickListener {

    private static final int REQUEST_IMAGE_CAPTURE = 1;
    private final static String PHOTO_NAME = "profile_photo.jpg";
    private final static String NAME_KEY = "fullname";
    Button cameraButton;
    ImageView photoProfile;
    EditText nameProfile;
    Bitmap photoFile;
    Boolean photoHasChanged = false;
    String fullname;

    public ProfileFragment() {
        // Required empty public constructor
    }

    public static ProfileFragment newInstance() {
        ProfileFragment fragment = new ProfileFragment();

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Get profile picture
        String file_path = getContext().getFilesDir().getAbsolutePath() + "/" + PHOTO_NAME;
        photoFile = BitmapFactory.decodeFile(file_path);

        SharedPreferences sharedPreferences = getActivity().getSharedPreferences("appstud_prodile", Context.MODE_PRIVATE);
        fullname = sharedPreferences.getString(NAME_KEY, "");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_profile, container, false);
        cameraButton = (Button) view.findViewById(R.id.cameraButton);
        photoProfile = (ImageView) view.findViewById(R.id.profile_photo);
        if(photoFile != null){
            photoProfile.setImageBitmap(photoFile);
        }
        nameProfile = (EditText) view.findViewById(R.id.profile_name);
        nameProfile.setText(fullname);

        cameraButton.setOnClickListener(this);

        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    private void dispatchTakePictureIntent() {

        // Launch camera
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(getContext().getPackageManager()) != null) {
            startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
        }

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == REQUEST_IMAGE_CAPTURE){
            if(resultCode == Activity.RESULT_OK){
                Bundle extras = data.getExtras();
                photoFile = (Bitmap) extras.get("data");
                photoProfile.setImageBitmap(photoFile);
                photoHasChanged = true;
            }
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){

            case R.id.cameraButton:
                dispatchTakePictureIntent();
                break;
        }
    }

    public void saveProfile(){

        if(photoHasChanged && photoFile != null){
            // Store photo
            String file_path = getContext().getFilesDir().getAbsolutePath() + "/" + PHOTO_NAME;
            File file = new File(file_path);
            if(!file.exists())
                file.mkdirs();

            FileOutputStream fOut = null;
            try {
                fOut = new FileOutputStream(file);
                photoFile.compress(Bitmap.CompressFormat.JPEG, 85, fOut);
                fOut.flush();
                fOut.close();
            } catch (FileNotFoundException e) {
                Log.e("onActivityResult", e.getMessage());
            } catch (IOException e) {
                Log.e("onActivityResult", e.getMessage());
            }
        }

        String name = nameProfile.getText().toString();
        SharedPreferences.Editor editor = getActivity().getSharedPreferences("appstud_prodile", Context.MODE_PRIVATE).edit();
        editor.putString(NAME_KEY, name);
        editor.commit();
    }
}
