package mm.appstud.com.appstud_molette_mathieu.helpers;

import android.content.Context;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

import mm.appstud.com.appstud_molette_mathieu.dao.RockstarDao;

/**
 * Created by Mathieu on 02/04/2016.
 */

public class DatabaseHelper {

    private MySQLiteOpenHelper mDbHelper;
    private final Context mContext;
    public static RockstarDao rockstarDao;


    public DatabaseHelper open() throws SQLException {
        mDbHelper = new MySQLiteOpenHelper(mContext);
        SQLiteDatabase mDb = mDbHelper.getWritableDatabase();

        rockstarDao = new RockstarDao(mDb);

        return this;
    }

    public void close() {
        mDbHelper.close();
    }

    public DatabaseHelper(Context context) {
        this.mContext = context;
    }
}
