package mm.appstud.com.appstud_molette_mathieu.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import mm.appstud.com.appstud_molette_mathieu.R;
import mm.appstud.com.appstud_molette_mathieu.adapters.RockstarsRecyclerViewAdapter;
import mm.appstud.com.appstud_molette_mathieu.helpers.DatabaseHelper;
import mm.appstud.com.appstud_molette_mathieu.interfaces.IRockstarListener;
import mm.appstud.com.appstud_molette_mathieu.managers.RockstarsManager;
import mm.appstud.com.appstud_molette_mathieu.models.Rockstar;

public class RockstarFavoritesFragment extends Fragment {

    IRockstarListener mListener;
    RockstarsRecyclerViewAdapter recyclerViewAdapter;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public RockstarFavoritesFragment() {
    }

    public static RockstarFavoritesFragment newInstance() {
        RockstarFavoritesFragment fragment = new RockstarFavoritesFragment();

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        recyclerViewAdapter = new RockstarsRecyclerViewAdapter(RockstarsManager.getInstance().getFavoriteRockstars(), true, mListener);
    }

    @Override
    public void onResume() {
        super.onResume();

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_rockstar_favorite_list, container, false);

        // Set the adapter
        if (view instanceof RecyclerView) {
            Context context = view.getContext();
            RecyclerView recyclerView = (RecyclerView) view;
            recyclerView.setLayoutManager(new LinearLayoutManager(context));
            recyclerView.setAdapter(recyclerViewAdapter);
        }
        return view;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            mListener = (IRockstarListener) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString()
                    + " must implement IRockstarListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

}
