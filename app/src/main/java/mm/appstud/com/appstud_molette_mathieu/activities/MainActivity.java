package mm.appstud.com.appstud_molette_mathieu.activities;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SearchView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import mm.appstud.com.appstud_molette_mathieu.R;
import mm.appstud.com.appstud_molette_mathieu.adapters.RockstarsRecyclerViewAdapter;
import mm.appstud.com.appstud_molette_mathieu.fragments.ProfileFragment;
import mm.appstud.com.appstud_molette_mathieu.fragments.RockstarFavoritesFragment;
import mm.appstud.com.appstud_molette_mathieu.fragments.RockstarsFragment;
import mm.appstud.com.appstud_molette_mathieu.interfaces.IRockstarListener;
import mm.appstud.com.appstud_molette_mathieu.managers.RockstarsManager;
import mm.appstud.com.appstud_molette_mathieu.models.Rockstar;

public class MainActivity extends AppCompatActivity implements ViewPager.OnPageChangeListener, IRockstarListener, SearchView.OnQueryTextListener {

    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link android.support.v4.app.FragmentStatePagerAdapter}.
     */
    private SectionsPagerAdapter mSectionsPagerAdapter;

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    private ViewPager mViewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);
        mViewPager.addOnPageChangeListener(this);
        mViewPager.setOffscreenPageLimit(2);

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(mViewPager);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.search_menu, menu);

        SearchManager searchManager =
                (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        SearchView searchView =
                (SearchView) menu.findItem(R.id.search).getActionView();
        searchView.setSearchableInfo(
                searchManager.getSearchableInfo(getComponentName()));
        searchView.setOnQueryTextListener((SearchView.OnQueryTextListener) this);


        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        // Hide or show search view
        int page = mViewPager.getCurrentItem();
        switch (page){

            case 0:
                menu.findItem(R.id.search).setVisible(true);
                menu.findItem(R.id.saveProfile).setVisible(false);
                break;

            case 1:
                menu.findItem(R.id.search).setVisible(false);
                menu.findItem(R.id.saveProfile).setVisible(false);
                break;

            case 2:
                menu.findItem(R.id.search).setVisible(false);
                menu.findItem(R.id.saveProfile).setVisible(true);
                break;
        }

        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){

            case R.id.saveProfile:
                Fragment fragment = getFragmentOfClass(ProfileFragment.class);
                if(fragment != null){
                    ((ProfileFragment)fragment).saveProfile();
                    Toast.makeText(this, getString(R.string.profile_saved), Toast.LENGTH_SHORT).show();
                }
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
        invalidateOptionsMenu();
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    @Override
    public void onRockstarBookmarked(Rockstar rockstar) {
        // Refresh view pager
        mSectionsPagerAdapter.notifyDataSetChanged();
    }

    @Override
    public void onFavoriteRemoved(Rockstar rockstar) {
        // Refresh view pager
        mSectionsPagerAdapter.notifyDataSetChanged();
    }

    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentStatePagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {

            Fragment fragment = null;
            switch (position){

                case 0:
                    fragment = RockstarsFragment.newInstance();
                    break;

                case 1:
                    fragment = RockstarFavoritesFragment.newInstance();
                    break;

                case 2:
                    fragment = ProfileFragment.newInstance();
                    break;
            }

            return fragment;
        }

        @Override
        public int getCount() {

            return getResources().getStringArray(R.array.main_tabs).length;
        }

        @Override
        public CharSequence getPageTitle(int position) {

            String[] tabs = getResources().getStringArray(R.array.main_tabs);

            return tabs[position];
        }

        @Override
        public int getItemPosition(Object object) {
            Class className = RockstarsFragment.class;
            int result = POSITION_UNCHANGED;
            switch (mViewPager.getCurrentItem()){

                case 0:
                    className = RockstarsFragment.class;
                    break;

                case 1:
                    className = RockstarFavoritesFragment.class;
                    break;

                case 2:
                    className = ProfileFragment.class;
                    break;
            }

            if(!object.getClass().equals(className)){
                if(object instanceof  RockstarFavoritesFragment || object instanceof RockstarsFragment){
                    result = POSITION_NONE;
                }
            }

            return result;
        }
    }


    @Override
    public boolean onQueryTextChange(String query) {

        return false;
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        Fragment fragment = getFragmentOfClass(RockstarsFragment.class);
        if(fragment != null){
            ((RockstarsFragment)fragment).getRecyclerViewAdapter().getFilter().filter(query);
        }
        return false;
    }

    private Fragment getFragmentOfClass(Class name){

        Fragment fragment = null;
        Boolean found = false;
        int i = 0;
        List<Fragment> fragments = getSupportFragmentManager().getFragments();
        while (fragment == null && i < fragments.size()){
            if(fragments.get(i).getClass().equals(name)){
                fragment = fragments.get(i);
            }
            i++;
        }

        return fragment;
    }
}
