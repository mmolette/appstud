package mm.appstud.com.appstud_molette_mathieu;

import android.app.Application;
import android.content.Context;

import mm.appstud.com.appstud_molette_mathieu.helpers.DatabaseHelper;
import mm.appstud.com.appstud_molette_mathieu.helpers.MySQLiteOpenHelper;

/**
 * Created by Mathieu on 07/02/2016.
 */
public class AppStud extends Application {

    private static Context mContext;
    public static DatabaseHelper mDb;

    @Override
    public void onCreate() {
        super.onCreate();
        mContext = this;
        mDb = new DatabaseHelper(this);
        mDb.open();
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
        mDb.close();
    }

    public static Context getContext(){
        return mContext;
    }
}
