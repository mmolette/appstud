package mm.appstud.com.appstud_molette_mathieu.managers;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import mm.appstud.com.appstud_molette_mathieu.AppStud;

/**
 * Created by Mathieu on 13/02/2016.
 */
public class NetworkManager {

    private static NetworkManager instance = null;

    protected NetworkManager() {
        // Exists only to defeat instantiation.
    }
    public static NetworkManager getInstance() {
        if(instance == null) {
            instance = new NetworkManager();
        }
        return instance;
    }

    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) AppStud.getContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }
}
