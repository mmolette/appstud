package mm.appstud.com.appstud_molette_mathieu.tasks;

import android.os.AsyncTask;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;

import org.json.JSONObject;

import java.util.List;

import mm.appstud.com.appstud_molette_mathieu.adapters.RockstarsRecyclerViewAdapter;
import mm.appstud.com.appstud_molette_mathieu.managers.RockstarsManager;
import mm.appstud.com.appstud_molette_mathieu.models.Rockstar;
import mm.appstud.com.appstud_molette_mathieu.services.RockstarServices;

/**
 * Created by Mathieu on 02/04/2016.
 */
public class RefreshRockstarsAsyncTask extends AsyncTask<Void, Void, List<Rockstar>> {

    private JSONObject response;
    private RockstarsRecyclerViewAdapter recyclerViewAdapter;
    private SwipeRefreshLayout swipeRefreshLayout;

    public RefreshRockstarsAsyncTask(JSONObject response, RockstarsRecyclerViewAdapter recyclerViewAdapter, SwipeRefreshLayout layout) {
        this.response = response;
        this.recyclerViewAdapter = recyclerViewAdapter;
        this.swipeRefreshLayout = layout;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        recyclerViewAdapter.clear();
        recyclerViewAdapter.notifyDataSetChanged();
    }

    @Override
    protected List<Rockstar> doInBackground(Void... params) {
        RockstarsManager.getInstance().parseContacts(this.response);
        return RockstarsManager.getInstance().getRockstarsList();
    }

    @Override
    protected void onPostExecute(List<Rockstar> rockstars) {
        super.onPostExecute(rockstars);

        recyclerViewAdapter.notifyDataSetChanged();
        Log.i("onRefresh", "recyclerViewAdapter updated");
        // Refresh is finish
        swipeRefreshLayout.setRefreshing(false);
    }
}
