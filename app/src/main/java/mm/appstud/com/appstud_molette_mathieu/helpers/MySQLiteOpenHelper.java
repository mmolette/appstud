package mm.appstud.com.appstud_molette_mathieu.helpers;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class MySQLiteOpenHelper extends SQLiteOpenHelper {

    public static final String TABLE_ROCKSTARS = "rockstars";
    public static final String COLUMN_ID = "_id";
    public static final String COLUMN_LASTNAME = "lastname";
    public static final String COLUMN_FIRSTNAME = "firstname";
    public static final String COLUMN_STATUS = "status";
    public static final String COLUMN_PICTURE = "picture";
    public static final String[] ROCKSTAR_COLUMNS = new String[] { COLUMN_ID,
            COLUMN_LASTNAME, COLUMN_FIRSTNAME, COLUMN_STATUS, COLUMN_PICTURE };

    private static final String DATABASE_NAME = "appstud.db";
    private static final int DATABASE_VERSION = 1;

    // Database creation sql statement
    private static final String DATABASE_CREATE = "create table "
            + TABLE_ROCKSTARS + "(" + COLUMN_ID
            + " integer primary key autoincrement, " + COLUMN_LASTNAME
            + " text not null, " + COLUMN_FIRSTNAME
            + " text not null, " + COLUMN_STATUS
            + " text not null, " + COLUMN_PICTURE
            + " text not null);";

    public MySQLiteOpenHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase database) {
        database.execSQL(DATABASE_CREATE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Log.w(MySQLiteOpenHelper.class.getName(),
                "Upgrading database from version " + oldVersion + " to "
                        + newVersion + ", which will destroy all old data");
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_ROCKSTARS);
        onCreate(db);
    }

}