package mm.appstud.com.appstud_molette_mathieu.dao;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteConstraintException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import mm.appstud.com.appstud_molette_mathieu.helpers.MySQLiteOpenHelper;
import mm.appstud.com.appstud_molette_mathieu.interfaces.IRockstarDao;
import mm.appstud.com.appstud_molette_mathieu.models.Rockstar;
import mm.appstud.com.appstud_molette_mathieu.providers.DbContentProvider;

/**
 * Created by Mathieu on 02/04/2016.
 */
public class RockstarDao extends DbContentProvider
        implements IRockstarDao {

    private Cursor cursor;
    private ContentValues initialValues;
    public RockstarDao(SQLiteDatabase db) {
        super(db);
    }
    public Rockstar fetchRockstarById(int id) {
        final String selectionArgs[] = { String.valueOf(id) };
        final String selection = MySQLiteOpenHelper.COLUMN_ID + " = ?";
        Rockstar rockstar = new Rockstar();
        cursor = super.query(MySQLiteOpenHelper.TABLE_ROCKSTARS, MySQLiteOpenHelper.ROCKSTAR_COLUMNS, selection,
                selectionArgs, MySQLiteOpenHelper.COLUMN_ID);
        if (cursor != null) {
            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {
                rockstar = cursorToEntity(cursor);
                cursor.moveToNext();
            }
            cursor.close();
        }

        return rockstar;
    }

    public List<Rockstar> fetchAllRockstars() {
        List<Rockstar> rockstarList = new ArrayList<Rockstar>();
        cursor = super.query(MySQLiteOpenHelper.TABLE_ROCKSTARS, MySQLiteOpenHelper.ROCKSTAR_COLUMNS, null,
                null, MySQLiteOpenHelper.COLUMN_ID);

        if (cursor != null) {
            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {
                Rockstar rockstar = cursorToEntity(cursor);
                rockstarList.add(rockstar);
                cursor.moveToNext();
            }
            cursor.close();
        }

        return rockstarList;
    }

    public boolean addRockstar(Rockstar Rockstar) {
        // set values
        setContentValue(Rockstar);
        try {
            return super.insert(MySQLiteOpenHelper.TABLE_ROCKSTARS, getContentValue()) > 0;
        } catch (SQLiteConstraintException ex){
            Log.w("Database", ex.getMessage());
            return false;
        }
    }

    @Override
    public boolean deleteRockstar(Rockstar rockstar) {

        final String selectionArgs[] = { String.valueOf(rockstar.getId()) };
        final String selection = MySQLiteOpenHelper.COLUMN_ID + " = ?";
        try {
            return super.delete(MySQLiteOpenHelper.TABLE_ROCKSTARS, selection, selectionArgs) > 0;
        } catch (SQLiteConstraintException ex){
            Log.w("Database", ex.getMessage());
            return false;
        }
    }

    protected Rockstar cursorToEntity(Cursor cursor) {

        Rockstar rockstar = new Rockstar();

        int idIndex;
        int lastnameIndex;
        int firstnameIndex;
        int statusIndex;
        int pictureIndex;

        if (cursor != null) {

            if (cursor.getColumnIndex(MySQLiteOpenHelper.COLUMN_ID) != -1) {
                idIndex = cursor.getColumnIndexOrThrow(
                        MySQLiteOpenHelper.COLUMN_ID);
                rockstar.setId(Long.valueOf(cursor.getString(idIndex)));
            }
            if (cursor.getColumnIndex(MySQLiteOpenHelper.COLUMN_LASTNAME) != -1) {
                lastnameIndex = cursor.getColumnIndexOrThrow(
                        MySQLiteOpenHelper.COLUMN_LASTNAME);
                rockstar.setLastname(cursor.getString(lastnameIndex));
            }
            if (cursor.getColumnIndex(MySQLiteOpenHelper.COLUMN_FIRSTNAME) != -1) {
                firstnameIndex = cursor.getColumnIndexOrThrow(
                        MySQLiteOpenHelper.COLUMN_FIRSTNAME);
                rockstar.setFirstname(cursor.getString(firstnameIndex));
            }
            if (cursor.getColumnIndex(MySQLiteOpenHelper.COLUMN_STATUS) != -1) {
                statusIndex = cursor.getColumnIndexOrThrow(MySQLiteOpenHelper.COLUMN_STATUS);
                rockstar.setStatus(cursor.getString(statusIndex));
            }
            if (cursor.getColumnIndex(MySQLiteOpenHelper.COLUMN_PICTURE) != -1) {
                pictureIndex = cursor.getColumnIndexOrThrow(MySQLiteOpenHelper.COLUMN_PICTURE);
                rockstar.setPicture(cursor.getString(pictureIndex));
            }

        }
        return rockstar;
    }

    private void setContentValue(Rockstar rockstar) {
        initialValues = new ContentValues();
        initialValues.put(MySQLiteOpenHelper.COLUMN_LASTNAME, rockstar.getLastname());
        initialValues.put(MySQLiteOpenHelper.COLUMN_FIRSTNAME, rockstar.getFirstname());
        initialValues.put(MySQLiteOpenHelper.COLUMN_STATUS, rockstar.getStatus());
        initialValues.put(MySQLiteOpenHelper.COLUMN_PICTURE, rockstar.getPicture());
    }

    private ContentValues getContentValue() {
        return initialValues;
    }

}

