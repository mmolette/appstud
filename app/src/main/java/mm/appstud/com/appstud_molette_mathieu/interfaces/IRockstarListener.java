package mm.appstud.com.appstud_molette_mathieu.interfaces;

import mm.appstud.com.appstud_molette_mathieu.models.Rockstar;

/**
 * Created by Mathieu on 03/04/2016.
 */
public interface IRockstarListener {

    public void onRockstarBookmarked(Rockstar rockstar);
    public void onFavoriteRemoved(Rockstar rockstar);
}
