package mm.appstud.com.appstud_molette_mathieu.activities;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import com.android.volley.Response;

import org.json.JSONObject;

import mm.appstud.com.appstud_molette_mathieu.R;
import mm.appstud.com.appstud_molette_mathieu.managers.RockstarsManager;

public class SplashscreenActivity extends AppCompatActivity {

    private static final int PROP_DELAY = 2000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splashscreen);

        Response.Listener<JSONObject> resListener = new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
                Log.d("onResponse", response.toString());

                RockstarsManager.getInstance().parseContacts(response);
                // Launch main activity after delay
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {

                        Intent intent = new Intent(SplashscreenActivity.this, MainActivity.class);
                        startActivity(intent);
                        finish();
                    }
                }, PROP_DELAY);
            }
        };

        RockstarsManager.getInstance().loadRockstars(resListener);
    }
}
