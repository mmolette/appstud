package mm.appstud.com.appstud_molette_mathieu.managers;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.Adapter;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import mm.appstud.com.appstud_molette_mathieu.AppStud;
import mm.appstud.com.appstud_molette_mathieu.R;
import mm.appstud.com.appstud_molette_mathieu.adapters.RockstarsRecyclerViewAdapter;
import mm.appstud.com.appstud_molette_mathieu.helpers.DatabaseHelper;
import mm.appstud.com.appstud_molette_mathieu.helpers.VolleyHelper;
import mm.appstud.com.appstud_molette_mathieu.models.Rockstar;
import mm.appstud.com.appstud_molette_mathieu.services.RockstarServices;

/**
 * Created by Mathieu on 13/02/2016.
 */
public class RockstarsManager {

    private static RockstarsManager instance = null;
    private List<Rockstar> rockstarsList;
    private List<Rockstar> filteredList;
    private HashMap<String, Rockstar> favorites;

    protected RockstarsManager() {
        // Exists only to defeat instantiation.
    }
    public static RockstarsManager getInstance() {
        if(instance == null) {
            instance = new RockstarsManager();
            instance.favorites = new HashMap<>();
            instance.rockstarsList = new ArrayList<>();
            instance.filteredList = new ArrayList<>();
        }
        return instance;
    }

    public void loadRockstars(Response.Listener<JSONObject> listener){

        JsonObjectRequest rockstarsRequest = RockstarServices.getRockstarsRequest(listener);
        // Launch request
        VolleyHelper.getInstance(AppStud.getContext()).addToRequestQueue(rockstarsRequest);
    }

    public List<Rockstar> getRockstarsList() {

        return this.rockstarsList;
    }

    public List<Rockstar> getFilteredList() {

        return this.filteredList;
    }

    public void setRockstarsList(List<Rockstar> rockstars){
        this.rockstarsList = rockstars;
    }

    public void resetFilteredList(){
        this.filteredList.clear();
        this.filteredList.addAll(this.rockstarsList);
    }

    public void parseContacts(JSONObject response){

        Log.i("parsContacts", "parsing json contacts");
        this.rockstarsList.clear();
        if(response.has("contacts")) {
            try {
                JSONArray contacts = response.getJSONArray("contacts");
                if (contacts.length() > 0) {

                    for (int i = 0; i < contacts.length(); i++) {
                        JSONObject contact = (JSONObject) contacts.get(i);
                        Rockstar rockstar = new Rockstar(contact);
                        Log.v("parseContacts", rockstar.getPicture());
                        this.rockstarsList.add(rockstar);
                        this.filteredList.add(rockstar);
                    }
                }
            } catch (JSONException e) {
                Log.e("onResponse", e.toString());
                Toast.makeText(AppStud.getContext(), "Error: " + e.getMessage(), Toast.LENGTH_LONG).show();
            }
        }
    }

    public boolean isFavorite(Rockstar rockstar){

        return  favorites.containsKey(rockstar.getLastname());
    }

    public List<Rockstar> getFavoriteRockstars(){

        List<Rockstar> bookmarks = DatabaseHelper.rockstarDao.fetchAllRockstars();
        this.favorites.clear();
        for (Rockstar r : bookmarks) {
            this.favorites.put(r.getLastname(), r);
        }
        return bookmarks;
    }

    public boolean addFavorite(Rockstar rockstar){

        boolean result = false;
        if(!isFavorite(rockstar) && DatabaseHelper.rockstarDao.addRockstar(rockstar)){
            this.favorites.put(rockstar.getLastname(), rockstar);
            result = true;
        }

        return result;
    }

    public boolean removeFavorite(Rockstar rockstar){

        boolean result = false;
        if(isFavorite(rockstar) && DatabaseHelper.rockstarDao.deleteRockstar(rockstar)){
            this.favorites.remove(rockstar.getLastname());
            result = true;
        }

        return result;
    }
}
